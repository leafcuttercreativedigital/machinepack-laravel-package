# Laravel package for MachinePack

This package can be included in any Laravel project and will expose MachinePack
functionality as a [Laravel Package](https://laravel.com/docs/packages#service-providers)

## Routes

These are the routes that will be automatically added:

### /machinepack/1.0/send

sends a raw event directly to machinepack based on POST data:
```json
{
	"event": "path.to.your.event",
	"data": {
	  "some complex": "data"
	}
}
```

### /machinepack/1.0/jobstatus

gets an AsyncResult's status if any

### /machinepack/1.0/webhook

generates a 'webhook.request' event with these fields:
```json
{
        "Intangible/RESTRequest.server": {
		   "PHP_SERVER_VAR1": "VALUE",
		   ...
		},
        "Intangible/RESTRequest.data": {
		   "someUrlParam": "value"
		},
        "Intangible/RESTRequest.body": "RAW body contents of request (not decoded)"
}
```
