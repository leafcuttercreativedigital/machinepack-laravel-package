<?php

namespace MachinePack\Laravel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use MachinePack\Core\MachinePack as MP;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;

class MachinePack extends Controller
{
    const Version = '1.0';

    /**
     * Executes a raw MP send with input data, then returns standard Laravel Json API response.
     *
     * @param  array|Request $data raw event data.
     * @param  string        $prefix prefix to use in response messages.
     * @return Response
     */
    private function raw_send( $data, $prefix = '' ) {
        try {
            MP::init();

            if ( empty( $data['event'] ) || ! is_string( $data['event'] ) ) {
                return response()->json([
                    'message' => 'Request must include an "event" string containing the event being sent.'
                ], 400);
            }

            $results   = MP::send( $data['event'], (array) $data['data'] ?? [] );
            $http_code = 200;
            $output    = array_map(
                function ( $result ) use ( &$http_code ) {
                    $http_code = max( $http_code, $result->asHttpCode() );
                    return $result->asHttpResponseData();
                },
                $results
            );

            return response()->json($output, $http_code);
        } catch ( MissingArgumentsException $e ) {
            return response()->json([
                'message' => 'Missing event arguments.',
                'trace' => explode( "\n", (string) $e )
            ], 400);
        } catch ( \Throwable $e ) {
            return response()->json([
                'message' => 'Internal send API error.',
                'trace' => explode( "\n", (string) $e )
            ], 500);
        }
    }

    public function send(Request $request) {
        return $this->raw_send($request->all(), 'send');
    }

    public function jobstatus(Request $request) {
        return $this->raw_send($request->all(), 'jobstatus');
    }

    public function webhook(Request $request) {
        return $this->raw_send(
            [
                'event' => 'webhook.request',
                'data'  =>
                [
                    'Intangible/RESTRequest.data'   => $_REQUEST,
                    'Intangible/RESTRequest.server' => $_SERVER,
                ],
            ],
            'webhook'
        );
    }

    public function __construct() {
        $cfg = __DIR__ . '/../../../config/machinepack-default.yml';
        $file = getenv('MACHINEPACK_CONFIG');
        define('MACHINEPACK_CONFIG', is_file($file) ? $file : $cfg);
    }
}
