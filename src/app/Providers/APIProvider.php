<?php

namespace MachinePack\Laravel\Providers;

use Illuminate\Support\ServiceProvider;

class APIProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
    }
}
