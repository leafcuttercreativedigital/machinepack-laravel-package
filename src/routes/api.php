<?php
/**
 * Define machinepack API hooks
 */
Route::group(['namespace' => 'MachinePack\Laravel\Http\Controllers'], function() {
	Route::post('machinepack/' . \MachinePack\Laravel\Http\Controllers\MachinePack::Version . '/send', 'MachinePack@send');
	Route::post('machinepack/' . \MachinePack\Laravel\Http\Controllers\MachinePack::Version . '/jobstatus', 'MachinePack@jobstatus');
	Route::get('machinepack/' . \MachinePack\Laravel\Http\Controllers\MachinePack::Version . '/jobstatus', 'MachinePack@jobstatus');
	Route::post('machinepack/' . \MachinePack\Laravel\Http\Controllers\MachinePack::Version . '/webhook', 'MachinePack@webhook');
	Route::get('machinepack/' . \MachinePack\Laravel\Http\Controllers\MachinePack::Version . '/webhook', 'MachinePack@webhook');
});
